//
//  VATViewController.swift
//  pia6test
//
//  Created by Bill Martensson on 2017-03-21.
//  Copyright © 2017 Magic Technology. All rights reserved.
//

import UIKit

class VATViewController: UIViewController {

    
    
    @IBOutlet weak var priceTextfield: UITextField!
    
    @IBOutlet weak var addVATSwitch: UISwitch!
    
    @IBOutlet weak var result25Label: UILabel!
    
    @IBOutlet weak var result12Label: UILabel!
    
    @IBOutlet weak var result6Label: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func calculateVAT(_ sender: Any) {
        
        let vatCalc = VAT()
        
        let result25 = vatCalc.calculateVAT(price: priceTextfield.text!, VATpercent: 25, addVAT: addVATSwitch.isOn)

        let result12 = vatCalc.calculateVAT(price: priceTextfield.text!, VATpercent: 12, addVAT: addVATSwitch.isOn)

        let result6 = vatCalc.calculateVAT(price: priceTextfield.text!, VATpercent: 6, addVAT: addVATSwitch.isOn)

        if(result25 == nil)
        {
            result25Label.text = ""
            result12Label.text = ""
            result6Label.text = ""
        } else {
            result25Label.text = String(result25!)
            result12Label.text = String(result12!)
            result6Label.text = String(result6!)
        }
        
        
        
        
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
