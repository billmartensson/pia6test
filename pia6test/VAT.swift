//
//  VAT.swift
//  pia6test
//
//  Created by Bill Martensson on 2017-03-21.
//  Copyright © 2017 Magic Technology. All rights reserved.
//

import Foundation

class VAT {
    
    
    func calculateVAT(price : String, VATpercent : Int, addVAT : Bool) -> Double?
    {
        
        if(price == "banan")
        {
            print("HEJ BANAN")
            return nil
        }
        
        
        let priceValue = Double(price)
        
        if(priceValue == nil)
        {
            return nil
        }
        
        // Lägg på 25% på 100
        // 100 * 1,25 dvs price * (VATpercent/100)+1
        
        var resultPrice : Double = 0.0
        
        if(addVAT == true)
        {
            resultPrice = priceValue! * ((Double(VATpercent)/100)+1)
        } else {
            resultPrice = priceValue! / ((Double(VATpercent)/100)+1)
        }
        
        // 87,45536748 => 87 
        // round(X * 100)/100
        // 8745,536748 => 8746 => 87,46
        resultPrice = round(resultPrice * 100)/100
        
        
        
        
        return resultPrice
    }
    
    
    
}
