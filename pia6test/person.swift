//
//  person.swift
//  pia6test
//
//  Created by Bill Martensson on 2017-03-14.
//  Copyright © 2017 Magic Technology. All rights reserved.
//

import Foundation

class person
{
    // MARK: Variabler här!!
    /**
     This is the persons first name
    */
    var firstname : String?
    var lastname : String?
    
    // MARK: -
    // MARK: Funktioner här!!
    
    /// Gets the personen full name
    /// Very good function
    ///
    /// - Returns: firstname + lastname
    func getName() -> String
    {
        // FIXME: Kolla strängarna
        //return firstname!+" "+lastname!
        
        if(firstname == nil || lastname == nil)
        {
            return ""
        }
        
        return firstname!+" "+lastname!
    }

    
    /**
     # Sets the persons name
     1. First thing
     2. Second thing
     
     *Remember to set the name*
     
     - Parameter fname: Persons firstname to set
     - Returns: True if value ok
    */
    func addName(fname : String?, lname : String?) -> Bool
    {
        if(fname == nil || lname == nil)
        {
            return false
        }
        
        firstname = fname
        lastname = lname
        
        return true
    }
    
    
    func tripleNumber(inNumber : Int) -> Int
    {
    
        var tripleValue = inNumber * 3
        
        if(tripleValue > 10)
        {
            tripleValue = 10
        }
        
        return tripleValue
    }
    
}
