//
//  VATTest.swift
//  pia6test
//
//  Created by Bill Martensson on 2017-03-21.
//  Copyright © 2017 Magic Technology. All rights reserved.
//

import XCTest

class VATTest: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
    func testVAT()
    {
        let vatCalc = VAT()
        
        let result25 = vatCalc.calculateVAT(price: "100", VATpercent: 25, addVAT: true)
        XCTAssert(result25 == 125)
        
        let result25remove = vatCalc.calculateVAT(price: "100", VATpercent: 25, addVAT: false)
        XCTAssert(result25remove == 80)
        
        let result12 = vatCalc.calculateVAT(price: "100", VATpercent: 12, addVAT: true)
        XCTAssert(result12 == 112)
        
        let result12remove = vatCalc.calculateVAT(price: "100", VATpercent: 12, addVAT: false)
        XCTAssert(result12remove == 89.29)

        let result6 = vatCalc.calculateVAT(price: "100", VATpercent: 6, addVAT: true)
        XCTAssert(result6 == 106)
        
        let result6remove = vatCalc.calculateVAT(price: "100", VATpercent: 6, addVAT: false)
        XCTAssert(result6remove == 94.34)
        
        
        let failText1 = vatCalc.calculateVAT(price: "", VATpercent: 25, addVAT: true)
        XCTAssert(failText1 == nil)
        
        let failText2 = vatCalc.calculateVAT(price: "xxxxx", VATpercent: 25, addVAT: true)
        XCTAssert(failText2 == nil)

        let failText3 = vatCalc.calculateVAT(price: "banan", VATpercent: 25, addVAT: true)
        XCTAssert(failText3 == nil)

        
    }
    
}
